terraform {
  backend "gcs" {
    bucket      = "doc-20210601152517"
    prefix      = "terraform/state"
    credentials = "account.json"
  }
}
