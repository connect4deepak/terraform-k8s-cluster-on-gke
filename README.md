# Terraform: Deploying Kubernetes Cluster on Google Cloud using GKE

### Pre-Requisites:

- Install Gcloud CLI
- Create Credentials
`gcloud auth application-default login`


### Instructions

Define env var to create project in Gcloud

```bash
export PROJECT_ID=doc-$(date +%Y%m%d%H%M%S)
gcloud projects create $PROJECT_ID
```

Create Service Account
```bash
gcloud iam service-accounts create devops-catalog --project $PROJECT_ID --display-name devops-catalog
```

To confirm creation:
```bash
gcloud iam service-accounts list --project $PROJECT_ID
```

Create key to use with service account
```bash
gcloud iam service-accounts keys create account.json --iam-account devops-catalog@$PROJECT_ID.iam.gserviceaccount.com --project $PROJECT_ID
```

To confirm creation:
```bash
gcloud iam service-accounts keys list --iam-account devops-catalog@$PROJECT_ID.iam.gserviceaccount.com --project $PROJECT_ID
```

Assign permissions (USING 'owner' ONLY IN LAB ENVIRONMENT! SET PERMISSIONS ACCORDING BEST PRACTICES IN DEV/PROD!):
```bash
gcloud projects add-iam-policy-binding $PROJECT_ID --member serviceAccount:devops-catalog@$PROJECT_ID.iam.gserviceaccount.com --role roles/owner
```

Export TF var
```bash
export TF_VAR_project_id=$PROJECT_ID
```

Var for the bucket
```bash
export TF_VAR_state_bucket=doc-$(date +%Y%m%d%H%M%S)
```

Enable Kubernetes API for the project
<br></br>

Check GKE version to use
```bash
gcloud container get-server-config --region us-east1 --project $PROJECT_ID
```

Exporting K8S using a given version
```bash
export K8S_VERSION=1.19.10-gke.1000
```

Retrieve credentials so TF can use them
```bash
export KUBECONFIG=$PWD/kubeconfig
```

Give admin permissions to the cluster
```bash
kubectl create clusterrolebinding cluster-admin-binding --clusterrole cluster-admin --user $(gcloud config get-value account)
```

Deploy TF
```bash
terraform apply --var k8s_version=$K8S_VERSION
```